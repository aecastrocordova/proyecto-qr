var express=require('express');
var requestJSON = require('request-json');
const URL_BASE='/apitechu/v1/';
const URLBASEDB= 'https://api.mlab.com/api/1/databases/techu23db/collections/';
const queryStringField = '&f={"_id":0}';
var app=express();
const cors = require('cors');

app.use(cors());
app.options('*', cors());

require('dotenv').config();
const API_KEY = '&apiKey=' + process.env.MLAB_API_KEY;
const API_KEYS = 'apiKey=' + process.env.MLAB_API_KEY;

// MÉTODO GET TODOS LOS USUARIOS
function getUsers(req,res){
            console.log('GET' + URL_BASE + 'usersM');
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            var queryString = 'f={"_id":0}&s={"last_name":1}&l=10';
            var url_total = URLBASEDB + 'user?' + API_KEYS;
            //console.log(url_total);
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {"mensaje":"Error obteniendo usuario"};
                  res.status(500);
                } else{
                  if(body.length > 0){
                    response = body;
                  } else{
                    response = {"mensaje":"Ningún elemento 'user'."};
                    res.status(404);
                  }
                }
                res.send(response);
              });
};

module.exports.getUsers = getUsers;

// MÉTODO GET POR ID DE USUARIO
function getUsersID(req,res){
            console.log('GET' + URL_BASE + 'usersM/:id');
            var id= req.params.id;
            var queryString = 'q={"id_Usuario":' + id + '}';
            //var queryStringField = 'f={"_id":0}&s={"last_name":1}&l=10';
            var url_total = URLBASEDB + 'user?' + queryString + queryStringField + API_KEY;
            //console.log(url_total);
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {"mensaje":"Error obteniendo usuario"};
                  res.status(500);
                } else{
                  if(body.length > 0){
                    response = body;
                  } else{
                    response = {"mensaje":"Ningún elemento 'user'."};
                    res.status(404);
                  }
                }
                console.log(response);
                res.send(response);
              });
};

module.exports.getUsersID = getUsersID;

// MÉTODO GET CUENTA POR ID DE USUARIO
function getAccountID(req,res){
            console.log('GET' + URL_BASE + 'account/:id');
            var id= req.params.id;
            var queryString = 'q={"Nuctaord":' + id + '}';
            //var queryStringField = 'f={"_id":0}&s={"last_name":1}&l=10';
            var url_total = URLBASEDB + 'CUENTA?' + queryString + queryStringField + API_KEY;
            //console.log(url_total);
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {"mensaje":"Error obteniendo cuenta"};
                  res.status(500);
                } else{
                  if(body.length > 0){
                    response = body;
                  } else{
                    response = {"mensaje":"Ningún elemento 'cuenta'."};
                    res.status(404);
                  }
                }
                console.log(response);
                res.send(response);
              });
};

module.exports.getAccountID = getAccountID;

// MÉTODO GET RETIRO QR TODOS LIMITE A 80
function getRetiroQR(req,res){
            console.log('GET' + URL_BASE + 'retiroQR');
            var queryString = 'l=80' + '&s={"Fecha":-1}';
            var url_total = URLBASEDB + 'c_patm_withdrawals?' + queryString + queryStringField + API_KEY;
            //console.log(url_total);
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {
                    "estado": 1,
                    "mensaje":"Error obteniendo retiroQR"
                  };
                  res.status(400);
                } else{
                  if(body.length > 0){
                    response = {
                      "estado": 0,
                      "datos":body
                    };
                    res.status(200);
                  } else{
                    response = {
                      "estado": 1,
                      "mensaje":"No existen elementos retiroQR"
                    };
                    res.status(404);
                  }
                }
                //console.log(response);
                res.send(response);
              });
};

module.exports.getRetiroQR = getRetiroQR;

// MÉTODO GET RETIRO QR POR CHECKSUM
function getRetiroQRChsum(req,res){
            console.log('GET' + URL_BASE + 'retiroQR/:chsum');

            var atm_id= req.body.atm_id;
            var chsum= req.body.chsum;
            var estado= req.body.estado;
            var queryString = 'q={"atm_id":"' + atm_id + '","checksum_id":"' + chsum + '","status_type":' + estado + '}';
            var queryStringFieldB = '&f={"_id":0,"mobile_number":1,"key_em_number":1,"email_address":1,"status_type":1,"status_desc":1,"atm_id":1,"checksum_id":1,"account_number":1,"customer_name":1,"operation_amount":1}';
            var url_total = URLBASEDB + 'c_patm_withdrawals?' + queryString + queryStringFieldB + API_KEY;
            //console.log(url_total);
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {
                    "estado": 1,
                    "mensaje":"Error obteniendo retiroQR"
                  };
                  res.status(400);
                } else{
                  if(body.length > 0){
                    response = {
                      "estado": 0,
                      "datos":body
                    };
                    res.status(200);
                  } else{
                    response = {
                      "estado": 1,
                      "mensaje":"No existe el retiroQR con los datos ingresados"
                    };
                    res.status(404);
                  }
                }
                //console.log(response);
                res.send(response);
              });
};

module.exports.getRetiroQRChsum = getRetiroQRChsum;

// MÉTODO GET RETIRO QR POR FECHA
function getRetiroQRFecha(req,res){
            //console.log('GET' + URL_BASE + 'retiroQR/:fecha');
            var fecha1= req.body.fecha1;
            var fecha2= req.body.fecha2;
            // console.log(fecha1);
            // console.log(fecha2);
            var mes= fecha1.substring(0,1)== '0' ? fecha1.substring(1,2) : fecha1.substring(0,2);
            var dia= fecha1.substring(2,3)== '0' ? fecha1.substring(3,4) : fecha1.substring(2,4);
            var ano= fecha1.substring(4,8);
            var fechat= mes + '/' + dia + '/' + ano;

            console.log(fechat + ' ' + fecha2);
            var queryString = 'q={"Fecha":' + '"' + fechat + '"' +'}';
            //var queryStringField = 'f={"_id":0}&s={"last_name":1}&l=10';
            var url_total = URLBASEDB + 'retiro?' + queryString + queryStringField + API_KEY;
            //console.log(url_total);
            var httpClient = requestJSON.createClient(URLBASEDB);
            //console.log("Cliente HTTP mLAB creado");
            httpClient.get(url_total,
              function(err,respuestaMLAB,body){
                var response = {};
                if(err){
                  response = {"mensaje":"Error obteniendo retiroQR"};
                  res.status(500);
                } else{
                  if(body.length > 0){
                    response = body;
                  } else{
                    response = {"mensaje":"No existe el retiroQR con los datos ingresados"};
                    res.status(404);
                  }
                }
                //console.log(response);
                res.send(response);
              });
};

module.exports.getRetiroQRFecha = getRetiroQRFecha;

// MÉTODO POST REGISTRO NUEVO RETIRO QR
function setRetiroQR(req, res){
   var clienteMlab = requestJSON.createClient(URLBASEDB);
   var url_total = URLBASEDB + 'c_patm_withdrawals?' + queryStringField + API_KEY;
   //console.log(url_total);
   let date = new Date();
   let day = date.getDate();
   let month = date.getMonth() + 1;
   if(day<10){
     day = '0' + day;
   }
   if(month<10){
     month = '0' + month;
   }
   let year = date.getFullYear();
   let fechatot= day + '/' + month + '/' + year;
   //console.log(req.body.checksum_id);
   clienteMlab.get(url_total,
     function(error, respuestaMLab, body){
       var newRetiro = {
         "atm_id" : req.body.atm_id,
         "checksum_id" : req.body.checksum_id,
         "account_number" : req.body.account_number,
         "account_currency_code" : req.body.account_currency_code,
         "account_type" : req.body.account_type,
         "account_type_desc" : req.body.account_type_desc,
         "customer_name" : req.body.customer_name,
         "operation_amount" : req.body.operation_amount,
         "operation_currency_code" : req.body.operation_currency_code,
         "email_address" : req.body.email_address,
         "mobile_number" : req.body.mobile_number,
         "key_em_number" : req.body.key_em_number,
         "status_type" : req.body.status_type,
         "status_desc" : "Pendiente",
         "operation_date": fechatot
       };
       //console.log(newRetiro);
       clienteMlab.post(URLBASEDB + "c_patm_withdrawals?" + API_KEY, newRetiro,
         function(error, respuestaMLab, body){
           //console.log(body);
           if(error){
             response = {
               "estado": 1,
               "mensaje":"Error en ingreso de retiroQR"
             };
             res.status(400);
           } else{
             response = {
               "estado": 0,
               "mensaje":"retiroQR creado con éxito",
               "datos":body
             };
           res.status(201);
         }
          res.send(response);
         });
     });
 };

module.exports.setRetiroQR = setRetiroQR;

// MÉTODO PUT con id de mLab (_id.$oid)
function updateRetiroQR(req, res) {
     var atm_id= req.body.atm_id;
     var chsum= req.body.chsum;
     let userBody = req.body.datos;
     let respon = {};
     var queryString = 'q={"atm_id":"' + atm_id + '","checksum_id":"' + chsum + '"}';
     var httpClient = requestJSON.createClient(URLBASEDB);
     var url_total = URLBASEDB + 'c_patm_withdrawals?' + queryString + API_KEY;
     //console.log(url_total);
     httpClient.get(url_total,
       function(err, respuestaMLab, body){
         let response = body[0];
         // console.log(response);
         // console.log(userBody);
         if(body.length>0){
           var updatedUser = {};
           Object.keys(response).forEach(key => updatedUser[key] = response[key]);
           Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
           //console.log(updatedUser);
           // PUT a mLab
           httpClient.put(URLBASEDB + 'c_patm_withdrawals/' + response._id.$oid + '?' + API_KEYS, updatedUser,
             function(err, respuestaMLab, body){
               //console.log(body);
               if(err) {
                   respon = {
                     "estado" : 1,
                     "mensaje" : "Error actualizando retiroQR"
                   }
                   res.status(400);
               } else {
                   respon = {
                     "estado" : 0,
                     "mensaje" : "RetiroQR actualizado correctamente."
                   }
                   res.status(200);
               }
                res.send(respon);
             });
         }else{
           respon = {
             "estado" : 1,
             "mensaje" : "RetiroQR no existe"
           }
           res.status(500);
           res.send(respon);
         }

       });
 };

 module.exports.updateRetiroQR = updateRetiroQR;

//MÉTODO DELETE user with id
function deleteRetiroQR(req, res){
   var atm_id= req.body.atm_id;
   var chsum= req.body.chsum;
   let respon = {};
   var queryStringID = 'q={"atm_id":' + atm_id + ',"checksum_id":' + chsum + '}';
   console.log(URLBASEDB + 'c_patm_withdrawals?' + queryStringID + API_KEY);
   var httpClient = requestJSON.createClient(URLBASEDB);
   httpClient.get(URLBASEDB + 'c_patm_withdrawals?' +  queryStringID + API_KEY,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       console.log(respuesta);
       if(respuesta!=undefined){
         httpClient.delete(URLBASEDB + "c_patm_withdrawals/" + respuesta._id.$oid +'?'+ API_KEY,
           function(error, respuestaMLab,body){
             if(error) {
                 respon = {
                   "estado" : 1,
                   "mensaje" : "Error eliminando retiroQR"
                 }
                 res.status(400);
             } else {
                 respon = {
                   "estado" : 0,
                   "mensaje" : "RetiroQR eliminado correctamente."
                 }
                 res.status(200);
             }
              res.send(respon);
         });
       }else{
         respon = {
           "estado" : 1,
           "mensaje" : "RetiroQR no existe"
         }
         res.status(500);
         res.send(respon);
       }
     });
 };

 module.exports.deleteRetiroQR = deleteRetiroQR;

//MÉTODO POST login
function loginUser(req, res){
   let email = req.body.email;
   let pass = req.body.password;
   let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}';
   //let limFilter = 'l=1&';
   let clienteMlab = requestJSON.createClient(URLBASEDB); // Existe un usuario que cumple 'queryString'
   let login = '{"$set":{"logged":true}}';
           clienteMlab.put(URLBASEDB + 'userAccount?' + queryString + API_KEY, JSON.parse(login),
           //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
             function(errPut, resPut, bodyPut) {
               var reslogin = bodyPut.n;
               //console.log(reslogin);
               if(reslogin==1){
                 res.send({'mensaje':'Login correcto', 'user':email});
                 res.status(200);
               }else{
                 res.send({'mensaje':'Alguno de los datos ingresado es incorrecto. Verifique e intente nuevamente.', 'user':email});
                 res.status(404);
               }
             });
};

module.exports.loginUser = loginUser;

//MÉTODO POST logout
function logoutUser(req, res){
   let email = req.body.email;
   let queryString = 'q={"email":"' + email + '"}';
   let clienteMlab = requestJSON.createClient(URLBASEDB);
   let logout = '{"$unset":{"logged":true}}';
   var url_total = URLBASEDB + 'userAccount?' + queryString + queryStringField + API_KEY;
   clienteMlab.get(url_total,
     function(err,respuestaMLAB,body){
       if(err){
         response = {"msg":"Error obteniendo usuario"};
         res.status(500);
       } else{
         if(body.length > 0){
           var response = body[0].logged;
           //console.log(response);
           if(response!=undefined){
             clienteMlab.put(URLBASEDB + 'userAccount?' + queryString + API_KEY, JSON.parse(logout),
               function(errPut, resPut, bodyPut) {
                 var reslogin = bodyPut.n;
                 //console.log(reslogin);
                 if(reslogin==1){
                   res.send({'msg':'Logout correcto', 'user':email});
                   res.status(200);
                 }else{
                   res.send({'msg':'Logout incorrecto', 'user':email});
                 }
               });
           }else{
             res.send({"mensaje":"Usuario no autenticado"});
             res.status(404);
           }
         } else{
           res.send({"mensaje":"No existe el usuario"});
           res.status(404);
         }
       }
     });
   //console.log(queryString);
   //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
};

module.exports.logoutUser = logoutUser;
