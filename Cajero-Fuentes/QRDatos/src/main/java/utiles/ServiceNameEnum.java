package utiles;

public enum ServiceNameEnum {
	CONSULTARRETIRO("01", "consultarRetiro"),
	ACTUALIZARRETIRO("02", "actualizarRetiro"); 

	private String codigo;
	private String descripcion;

	private ServiceNameEnum(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
