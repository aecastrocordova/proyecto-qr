package pe.bbva.aso.servicios.retiroqr.controller;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.bean.RetiroQRBean;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;

public abstract class BaseController {
	Logger logger = (Logger) LoggerFactory.getLogger(getClass());
	

	public RetiroQRBean consultarRetiroQR(RequestConsultaRQRBean filtro) throws ServiceExceptionBBVA {
		logger.debug("Controller consultar: inicio");
		 RetiroQRBean response = this.ejecutarConsulta(filtro);
			logger.debug("Controller: fin");
			return response;
		}
	public OperacionDto actualizarRetiroQR(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA {
		logger.debug("Controller update: inicio");
		OperacionDto response = this.ejecutarUpdate(filtro);
			logger.debug("Controller: fin");
			return response;
		}	
	
		abstract RetiroQRBean ejecutarConsulta(RequestConsultaRQRBean filtro) throws ServiceExceptionBBVA ;
		abstract OperacionDto ejecutarUpdate(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA;
	
}
