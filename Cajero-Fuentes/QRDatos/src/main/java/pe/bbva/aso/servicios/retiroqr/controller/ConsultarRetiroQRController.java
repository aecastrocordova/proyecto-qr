package pe.bbva.aso.servicios.retiroqr.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import ch.qos.logback.classic.Logger;
import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.bean.RetiroQRBean;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;
import pe.bbva.retiroqr.service.RetiroQRService;

@Controller
@Scope("prototype")
public class ConsultarRetiroQRController  extends BaseController {
	Logger logger = (Logger) LoggerFactory.getLogger(getClass());

	@Autowired
	private RetiroQRService retiroQRService;

	@Override
	public RetiroQRBean ejecutarConsulta(RequestConsultaRQRBean filtro) throws ServiceExceptionBBVA {
		logger.info("ejecutar: inicio");
		RetiroQRBean response = this.retiroQRService.buscarRetiroQRServicio(filtro);
		logger.debug("ejecutar: fin");
		return response;
	}

	@Override
	OperacionDto ejecutarUpdate(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA {
		logger.info("ejecutar: inicio");
		OperacionDto response = this.retiroQRService.actualizarRetiroQRServicio(filtro);
		logger.debug("ejecutar: fin");
		return response;
	}
}
