package pe.bbva.aso.servicios.retiroqr.config;

import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import ch.qos.logback.classic.Logger;
import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.aso.servicios.cliente.base.interceptor.SpringConfigurableMapper;
import pe.bbva.aso.servicios.cliente.base.resttemplate.ServiciosASOAutenticadoConfig;
import pe.bbva.aso.servicios.retiroqr.controller.ConsultarRetiroQRController;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.bean.RetiroQRBean;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;

@Configuration
@ComponentScan(
		basePackages = {"pe.bbva.aso.servicios.*;pe.bbva.retiroqr.*" },
		basePackageClasses = SpringConfigurableMapper.class)
@PropertySource({ 
	"classpath:/aso_servicios.properties",
	"classpath:/paas_servicio_retiroqr.properties" 
})
public class RetiroQRASOConfig extends ServiciosASOAutenticadoConfig {
	final static Logger logger = (Logger) LoggerFactory.getLogger(RetiroQRASOConfig.class);

	private static AnnotationConfigApplicationContext ctx;

	public RetiroQRBean consultarRetiroQR(RequestConsultaRQRBean filtro) throws ServiceExceptionBBVA {
		logger.debug("consultarRetiroQR enviar: inicio");
		RetiroQRBean listCustomers = null;
		ctx = new AnnotationConfigApplicationContext(RetiroQRASOConfig.class);
		ConsultarRetiroQRController consultarRetiroQRController = ctx.getBean(ConsultarRetiroQRController.class);		
		listCustomers = consultarRetiroQRController.consultarRetiroQR(filtro);
		logger.debug("consultarRetiroQR enviar: fin");
		return listCustomers;
	}
	
	public OperacionDto actualizarRetiroQR(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA {
		logger.debug("actualizarRetiroQR enviar: inicio");
		OperacionDto operacionDto = null;
		ctx = new AnnotationConfigApplicationContext(RetiroQRASOConfig.class);
		ConsultarRetiroQRController consultarRetiroQRController = ctx.getBean(ConsultarRetiroQRController.class);		
		operacionDto = consultarRetiroQRController.actualizarRetiroQR(filtro);
		logger.debug("actualizarRetiroQR enviar: fin");
		return operacionDto;
	}
}
