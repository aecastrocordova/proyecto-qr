package pe.bbva.retiroqr.rest.dao.impl;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;

import ch.qos.logback.classic.Logger;
import pe.bbva.aso.servicios.cliente.base.exception.ConnectionExceptionBBVA;
import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.aso.servicios.cliente.base.resttemplate.CustomRestTemplate;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;
import pe.bbva.retiroqr.dto.ResponseConsultaRetiroQRDto;
import pe.bbva.retiroqr.rest.RetiroQRRestDao;
import utiles.ServiceNameEnum;


@Repository
@Scope("prototype")
public class RetiroQRRestClientImpl implements RetiroQRRestDao{
	final Logger logger = (Logger) LoggerFactory.getLogger(getClass());

	@Autowired
	@Qualifier("restTemplateAutenticado")
	private CustomRestTemplate restTemplateAutenticado;
	
	@Autowired
	protected Environment env;
	
	private Gson json = new Gson();
	
	@Override
	public ResponseConsultaRetiroQRDto buscarRetiroQR(RequestConsultaRQRBean filtro) throws ServiceExceptionBBVA {
		logger.debug("RetiroQRRestClientImpl consultarretiroQR: inicio ");
		logger.debug("RetiroQRRestClientImpl consultarretiroQR: parameters request: " + json.toJson(filtro));
		
		String pathServicio = env.getProperty("paas.servicio.rest.retiroqr.get.url");		
		//String pathServicio = "http://techubbva.herokuapp.com/apitechu/v1/retiroQR/ch/";
		//Map<String, String> parametrosUrl = new HashMap<String, String>();	
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Content-Length", "81");
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

		/*HttpEntity<Object> httpEntity = new HttpEntity<Object>(filtro,headers);
		ParameterizedTypeReference<ResponseConsultaRetiroQRDto> typeRef = new ParameterizedTypeReference<ResponseConsultaRetiroQRDto>() {
		};*/
		//ResponseEntity<ResponseConsultaRetiroQRDto> respuesta = null;
		ResponseConsultaRetiroQRDto response = null;
		
		try {
			  ResponseEntity<ResponseConsultaRetiroQRDto> responseEntity = restTemplateAutenticado.exchange(pathServicio,
			             HttpMethod.POST, new HttpEntity<>(filtro, headers), ResponseConsultaRetiroQRDto.class);	
			   response = responseEntity.getBody();
			  System.out.println("ff " +response);
			  
		}catch(ConnectionExceptionBBVA e) {
			throw new ServiceExceptionBBVA(ServiceNameEnum.CONSULTARRETIRO,e,e.getCodigo(),e.getMessage());
		}
		
		logger.debug("ConsultaClienteAPGRestClientImpl consultarCliente: parameters response: " + json.toJson(response));
		
		return response;
	}




	@Override
	public OperacionDto actualizarRetiroQR(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA {
		logger.debug("RetiroQRRestClientImpl consultarretiroQR: inicio ");
		logger.debug("RetiroQRRestClientImpl consultarretiroQR: parameters request: " + json.toJson(filtro));
		
		String pathServicio = env.getProperty("paas.servicio.rest.retiroqr.update.url");		
		
		//Map<String, String> parametrosUrl = new HashMap<String, String>();	
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Content-Length", "81");
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

		/*HttpEntity<Object> httpEntity = new HttpEntity<Object>(filtro,headers);
		ParameterizedTypeReference<ResponseConsultaRetiroQRDto> typeRef = new ParameterizedTypeReference<ResponseConsultaRetiroQRDto>() {
		};*/
		//ResponseEntity<ResponseConsultaRetiroQRDto> respuesta = null;
        OperacionDto response = null;
		
		try {
			  ResponseEntity<OperacionDto> responseEntity = restTemplateAutenticado.exchange(pathServicio,
			             HttpMethod.PUT, new HttpEntity<>(filtro, headers), OperacionDto.class);	
			   response = responseEntity.getBody();
			  System.out.println("ff " +response);
			  
		}catch(ConnectionExceptionBBVA e) {
			throw new ServiceExceptionBBVA(ServiceNameEnum.CONSULTARRETIRO,e,e.getCodigo(),e.getMessage());
		}
		
		logger.debug("ConsultaClienteAPGRestClientImpl consultarCliente: parameters response: " + json.toJson(response));
		
		return response;
	}
	
}
