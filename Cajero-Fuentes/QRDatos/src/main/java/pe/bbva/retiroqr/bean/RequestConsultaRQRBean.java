package pe.bbva.retiroqr.bean;

public class RequestConsultaRQRBean {
	
	private String atm_id;
	private String chsum;
	private Integer estado;
	
	public String getAtm_id() {
		return atm_id;
	}
	public void setAtm_id(String atm_id) {
		this.atm_id = atm_id;
	}
	public String getChsum() {
		return chsum;
	}
	public void setChsum(String chsum) {
		this.chsum = chsum;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	
	
	
	
	
}
