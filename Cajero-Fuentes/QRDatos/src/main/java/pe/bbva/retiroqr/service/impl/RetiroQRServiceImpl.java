package pe.bbva.retiroqr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.bean.RetiroQRBean;
import pe.bbva.retiroqr.dao.RetiroQRDao;
import pe.bbva.retiroqr.dao.impl.RetiroQRDaoImpl;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;
import pe.bbva.retiroqr.dto.ResponseConsultaRetiroQRDto;
import pe.bbva.retiroqr.dto.RetiroQRDto;
import pe.bbva.retiroqr.rest.RetiroQRRestDao;
import pe.bbva.retiroqr.service.RetiroQRService;



@Service
@Scope("prototype")
public class RetiroQRServiceImpl implements RetiroQRService {
	

@Autowired
private RetiroQRRestDao retiroQRRestDao;

	@Override
	public RetiroQRBean buscarRetiroQRServicio(RequestConsultaRQRBean filtro) throws ServiceExceptionBBVA {
		RetiroQRBean retiroBean = new RetiroQRBean();
		ResponseConsultaRetiroQRDto retConsultaDto=	retiroQRRestDao.buscarRetiroQR(filtro);
		
	   if(retConsultaDto.getEstado()==0&&retConsultaDto.getDatos().size()>0) {
		    RetiroQRDto retDto =  retConsultaDto.getDatos().get(0);
			retiroBean.setExiste(true);
			retiroBean.setNombreCliente(retDto.getCustomer_name());
			retiroBean.setNumCelular(retDto.getMobile_number());
			retiroBean.setClaveEfectivoMovil(retDto.getKey_em_number());
			Integer montoInt = new Integer(retDto.getOperation_amount().concat("00"));
			System.out.println("resul montoInt " +montoInt);
			String resul=	String.format("%015d",montoInt);
			System.out.println("resul montoInt2 " +resul);
			retiroBean.setMonto(resul);
			retiroBean.setEstado(retDto.getStatus_desc());
			retiroBean.setMoneda("604");
			retiroBean.setCorreo(retDto.getEmail_address());
	
		}else {
		    retiroBean.setExiste(false);
		}
		return retiroBean; 
	}

	@Override
	public RetiroQRBean buscarRetiroQRPendiente(String numATM, String checksum) throws Exception {
		RetiroQRBean retiroBean = new RetiroQRBean();
		RetiroQRDao retiroDaoImpl = new RetiroQRDaoImpl();
		RetiroQRDto retDto= retiroDaoImpl.buscarRetiroQRPendiente(numATM, checksum);
		if(retDto!=null) {
			retiroBean.setExiste(true);
			retiroBean.setNombreCliente(retDto.getCustomer_name());
			retiroBean.setNumCelular(retDto.getMobile_number());
			retiroBean.setClaveEfectivoMovil(retDto.getKey_em_number());
			Integer montoInt = new Integer(retDto.getOperation_amount().concat("00"));
			System.out.println("resul montoInt " +montoInt);
			String resul=	String.format("%015d",montoInt);
			System.out.println("resul montoInt2 " +resul);
			retiroBean.setMonto(resul);
			retiroBean.setEstado(retDto.getStatus_desc());
			retiroBean.setMoneda("604");
			retiroBean.setCorreo(retDto.getEmail_address());
		}else {
		    retiroBean.setExiste(false);
		}
		return retiroBean;
	}

	@Override
	public RetiroQRBean buscarRetiroQR(String numATM, String checksum, Integer estado) throws Exception {
		RetiroQRBean retiroBean = new RetiroQRBean();
		RetiroQRDao retiroDaoImpl = new RetiroQRDaoImpl();
		RetiroQRDto retDto= retiroDaoImpl.buscarRetiroQR(numATM, checksum, estado);
		if(retDto!=null) {
			retiroBean.setExiste(true);
			retiroBean.setNombreCliente(retDto.getCustomer_name());
			retiroBean.setNumCelular(retDto.getMobile_number());
			retiroBean.setClaveEfectivoMovil(retDto.getKey_em_number());
			Integer montoInt = new Integer(retDto.getOperation_amount().concat("00"));
			String resul=	String.format("%015d",montoInt);
			System.out.println("resul " +resul);
			retiroBean.setMonto(resul);
			retiroBean.setEstado(retDto.getStatus_desc());
			retiroBean.setMoneda("604");
			retiroBean.setCorreo(retDto.getEmail_address());
		}else {
		    retiroBean.setExiste(false);
		}
		return retiroBean;
	}

	@Override
	public OperacionDto actualizarRetiroQR(String numATM, String checksum) throws Exception {
		RetiroQRDao retiroDaoImpl = new RetiroQRDaoImpl();
		return retiroDaoImpl.actualizarRetiroQR(numATM, checksum);
	}
	
	@Override
	public OperacionDto actualizarRetiroQRServicio(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA {
		OperacionDto retConsultaDto=	retiroQRRestDao.actualizarRetiroQR(filtro);
		return retConsultaDto;
	}

}
