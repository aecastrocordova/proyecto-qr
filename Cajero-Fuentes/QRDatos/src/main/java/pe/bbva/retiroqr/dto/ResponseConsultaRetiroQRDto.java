package pe.bbva.retiroqr.dto;

import java.util.List;

public class ResponseConsultaRetiroQRDto {
	
	private Integer estado;
	private List<RetiroQRDto> datos;
	
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public List<RetiroQRDto> getDatos() {
		return datos;
	}
	public void setDatos(List<RetiroQRDto> datos) {
		this.datos = datos;
	}
	
	

}
