package pe.bbva.retiroqr.dto;

public class RequestUpdateRetiroQRDto {
	
	private String atm_id;
	private String chsum;
	private EstadoUpdateDto datos;
	
	public String getAtm_id() {
		return atm_id;
	}
	public void setAtm_id(String atm_id) {
		this.atm_id = atm_id;
	}
	public String getChsum() {
		return chsum;
	}
	public void setChsum(String chsum) {
		this.chsum = chsum;
	}
	public EstadoUpdateDto getDatos() {
		return datos;
	}
	public void setDatos(EstadoUpdateDto datos) {
		this.datos = datos;
	}
	
	
	
	
	
	
	

}
