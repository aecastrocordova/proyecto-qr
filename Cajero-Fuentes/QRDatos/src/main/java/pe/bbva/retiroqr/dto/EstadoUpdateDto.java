package pe.bbva.retiroqr.dto;

public class EstadoUpdateDto {

	private Integer status_type;
	private String status_desc;
	public Integer getStatus_type() {
		return status_type;
	}
	public void setStatus_type(Integer status_type) {
		this.status_type = status_type;
	}
	public String getStatus_desc() {
		return status_desc;
	}
	public void setStatus_desc(String status_desc) {
		this.status_desc = status_desc;
	}
	
	
}
