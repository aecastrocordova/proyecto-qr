package pe.bbva.retiroqr.dao;

import com.mongodb.MongoClient;

import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RetiroQRDto;

public interface RetiroQRDao {
	 MongoClient crearConexion();
	 void cerrarConexion(MongoClient mongoClient );
	 RetiroQRDto buscarRetiroQRPendiente(String numATM, String checksum) throws Exception;
	 RetiroQRDto buscarRetiroQR(String numATM, String checksum,Integer estado) throws Exception;
	 public OperacionDto actualizarRetiroQR(String numATM, String checksum) throws Exception;
	 public OperacionDto insertarRetiroQR(String numATM, String checksum,Integer numCel,
             Integer claveem,String correo, Integer monto) throws Exception;
}
