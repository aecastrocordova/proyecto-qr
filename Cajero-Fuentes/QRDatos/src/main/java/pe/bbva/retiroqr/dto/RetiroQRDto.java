package pe.bbva.retiroqr.dto;

public class RetiroQRDto {

	private Object _id;
	private String atm_id;
	private Object checksum_id;
	private String mobile_number;
	private String key_em_number;
	private String operation_amount;
	private String operation_currency_code;
	private Object account_number;
	private String account_currency_code;
	private String account_balance_amount;
	private String account_type;
	private String account_type_desc;
	private String customer_name;
	private String email_address;
	private String status_type;
	private String status_desc;
	private String operation_date;
	private String operation_number;
	private String operation_identification_id;
	private String operation_description_desc;
	
	public Object get_id() {
		return _id;
	}
	public void set_id(Object _id) {
		this._id = _id;
	}
	public String getAtm_id() {
		return atm_id;
	}
	public void setAtm_id(String atm_id) {
		this.atm_id = atm_id;
	}
	public Object getChecksum_id() {
		return checksum_id;
	}
	public void setChecksum_id(Object checksum_id) {
		this.checksum_id = checksum_id;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getKey_em_number() {
		return key_em_number;
	}
	public void setKey_em_number(String key_em_number) {
		this.key_em_number = key_em_number;
	}
	public String getOperation_amount() {
		return operation_amount;
	}
	public void setOperation_amount(String operation_amount) {
		this.operation_amount = operation_amount;
	}
	public String getOperation_currency_code() {
		return operation_currency_code;
	}
	public void setOperation_currency_code(String operation_currency_code) {
		this.operation_currency_code = operation_currency_code;
	}
	public Object getAccount_number() {
		return account_number;
	}
	public void setAccount_number(Object account_number) {
		this.account_number = account_number;
	}
	public String getAccount_currency_code() {
		return account_currency_code;
	}
	public void setAccount_currency_code(String account_currency_code) {
		this.account_currency_code = account_currency_code;
	}
	public String getAccount_balance_amount() {
		return account_balance_amount;
	}
	public void setAccount_balance_amount(String account_balance_amount) {
		this.account_balance_amount = account_balance_amount;
	}
	public String getAccount_type() {
		return account_type;
	}
	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}
	public String getAccount_type_desc() {
		return account_type_desc;
	}
	public void setAccount_type_desc(String account_type_desc) {
		this.account_type_desc = account_type_desc;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getEmail_address() {
		return email_address;
	}
	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}
	public String getStatus_type() {
		return status_type;
	}
	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}
	public String getStatus_desc() {
		return status_desc;
	}
	public void setStatus_desc(String status_desc) {
		this.status_desc = status_desc;
	}
	public String getOperation_date() {
		return operation_date;
	}
	public void setOperation_date(String operation_date) {
		this.operation_date = operation_date;
	}
	public String getOperation_number() {
		return operation_number;
	}
	public void setOperation_number(String operation_number) {
		this.operation_number = operation_number;
	}
	public String getOperation_identification_id() {
		return operation_identification_id;
	}
	public void setOperation_identification_id(String operation_identification_id) {
		this.operation_identification_id = operation_identification_id;
	}
	public String getOperation_description_desc() {
		return operation_description_desc;
	}
	public void setOperation_description_desc(String operation_description_desc) {
		this.operation_description_desc = operation_description_desc;
	}
	
	
	
	
	
	
	
}
