package pe.bbva.retiroqr.service;

import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.bean.RetiroQRBean;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;

public interface RetiroQRService {
	
	 RetiroQRBean buscarRetiroQRPendiente(String numATM, String checksum) throws Exception;
	 RetiroQRBean buscarRetiroQR(String numATM, String checksum,Integer estado) throws Exception;
	 public OperacionDto actualizarRetiroQR(String numATM, String checksum) throws Exception;
	 
	 public RetiroQRBean buscarRetiroQRServicio(RequestConsultaRQRBean request) throws ServiceExceptionBBVA;
	OperacionDto actualizarRetiroQRServicio(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA;
}
