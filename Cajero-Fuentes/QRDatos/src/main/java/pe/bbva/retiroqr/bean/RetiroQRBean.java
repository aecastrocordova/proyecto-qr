package pe.bbva.retiroqr.bean;

public class RetiroQRBean {

	private boolean existe;
	private String monto;
	private String nombreCliente;
	private String numCelular;
	private String claveEfectivoMovil;
	private String moneda;
	private String estado;
	private String correo;
	private String mensaje;
	
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public boolean isExiste() {
		return existe;
	}
	public void setExiste(boolean existe) {
		this.existe = existe;
	}
	public String getNumCelular() {
		return numCelular;
	}
	public void setNumCelular(String numCelular) {
		this.numCelular = numCelular;
	}
	public String getClaveEfectivoMovil() {
		return claveEfectivoMovil;
	}
	public void setClaveEfectivoMovil(String claveEfectivoMovil) {
		this.claveEfectivoMovil = claveEfectivoMovil;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
	
	
}
