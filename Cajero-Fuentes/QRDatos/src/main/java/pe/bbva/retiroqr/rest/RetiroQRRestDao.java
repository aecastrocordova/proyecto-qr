package pe.bbva.retiroqr.rest;

import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;
import pe.bbva.retiroqr.dto.ResponseConsultaRetiroQRDto;

public interface RetiroQRRestDao {
	
	 ResponseConsultaRetiroQRDto buscarRetiroQR(RequestConsultaRQRBean filtro) throws ServiceExceptionBBVA;
	 OperacionDto actualizarRetiroQR(RequestUpdateRetiroQRDto filtro) throws ServiceExceptionBBVA;
}
