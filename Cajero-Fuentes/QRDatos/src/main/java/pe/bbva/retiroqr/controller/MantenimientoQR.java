package pe.bbva.retiroqr.controller;


import pe.bbva.retiroqr.bean.RetiroQRBean;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.service.RetiroQRService;
import pe.bbva.retiroqr.service.impl.RetiroQRServiceImpl;

public final class MantenimientoQR {
	
	
	public  RetiroQRBean buscarRetiroQR(String numATM, String checksum) {
		RetiroQRService retiroDaoImpl = new RetiroQRServiceImpl();
		
		RetiroQRBean sal = new RetiroQRBean();
		try {
			sal = retiroDaoImpl.buscarRetiroQRPendiente(numATM, checksum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sal;
	}
	
	public  RetiroQRBean buscarRetiroQRDummy(String numATM, String checksum) {
		RetiroQRBean sal = new RetiroQRBean();
		sal.setExiste(true);
		sal.setNombreCliente("Ana Paola");
		sal.setMonto("000000000006000");
		sal.setClaveEfectivoMovil("1234");
		sal.setNumCelular("994677155");
		sal.setMoneda("604");
		sal.setEstado("Pendiente");
		return sal;
	}

	public  OperacionDto actualizarRetiroQR(String numATM, String checksum) {
		RetiroQRService retiroDaoImpl = new RetiroQRServiceImpl();
		
		OperacionDto sal = new OperacionDto();
		try {
			sal = retiroDaoImpl.actualizarRetiroQR(numATM, checksum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sal;
	}
	
}
