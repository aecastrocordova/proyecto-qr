package pe.bbva.retiroqr.dao.impl;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;

import pe.bbva.retiroqr.dao.RetiroQRDao;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RetiroQRDto;
import pe.bbva.retiroqr.util.Constantes;

public class RetiroQRDaoImpl implements RetiroQRDao {
	  MongoClient mongoClient =null;
	 static MongoDatabase mdb =null;
	 
	   public MongoClient crearConexion() {
	     
	      
	        try {
	        	List<ServerAddress>seeds = new ArrayList<ServerAddress>();
				seeds.add(new ServerAddress(Constantes.IP_SERVIDOR+
						                    Constantes.DOS_PUNTOS+
						                    Constantes.PUERTO));
				MongoCredential credential = MongoCredential.createCredential(Constantes.CONEX_USER_NAME,
																			  Constantes.CONEX_DATA_BASE, 
						                                                      Constantes.CONEX_PASSWORD.toCharArray());
			
			    mongoClient = new MongoClient(seeds, Arrays.asList(credential));
			    obtenerMongoDatabase(mongoClient);
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	        return mongoClient;
	    }
	 
	 public  void cerrarConexion(MongoClient mongoClient ) {
		   mongoClient.close();
	 }
	 public MongoDatabase obtenerMongoDatabase(MongoClient mongoClient ) throws Exception {
		 return mdb= mongoClient.getDatabase( Constantes.CONEX_DATA_BASE);
	 }
	 
	  public RetiroQRDto buscarRetiroQRPendiente(String numATM, String checksum) throws Exception {
		  crearConexion();
		  RetiroQRDto retQRDto =null;
			System.out.println("Inicio buscarRetiroQR");
			//Bson filter = eq("cod_atm", numATM);
			Bson filter =and(eq("atm_id", numATM), eq("checksum_id", checksum), eq("status_type", 1));
			
			MongoCollection<Document> lstResultado= mdb.getCollection(Constantes.COLLECTION_RETIRO_QR);
			FindIterable<Document> fitd =lstResultado.find(filter).limit(5); 
			if(lstResultado!=null&&lstResultado.count()>0) {
				for (Document document : fitd) {
					System.out.println("iterable "+document.toJson());
				    retQRDto = new Gson().fromJson(document.toJson(), RetiroQRDto.class); 
					System.out.println("Cliente:  "+retQRDto.getCustomer_name());
				}
			}
            cerrarConexion(mongoClient);
			System.out.println("Fin buscarRetiroQR");
	    	return retQRDto;
		}
	  public RetiroQRDto buscarRetiroQR(String numATM, String checksum, Integer estado) throws Exception {
		     crearConexion();
			 RetiroQRDto retQRDto = null;
				System.out.println("Inicio buscarRetiroQR");
				//Bson filter = eq("cod_atm", numATM);
				Bson filter =and(eq("atm_id", numATM), eq("checksum_id", checksum), eq("status_type", estado));
				
				MongoCollection<Document> lstResultado= mdb.getCollection(Constantes.COLLECTION_RETIRO_QR);
				FindIterable<Document> fitd =lstResultado.find(filter).limit(5); 
				if(lstResultado!=null&&lstResultado.count()>0) {
					for (Document document : fitd) {
						System.out.println("iterable "+document.toJson());
					    retQRDto = new Gson().fromJson(document.toJson(), RetiroQRDto.class); 
						System.out.println("Cliente:  "+retQRDto.getCustomer_name());
					}
				}
				cerrarConexion(mongoClient);
				System.out.println("Fin buscarRetiroQR");
		    	return retQRDto;
			}
	  
	  public OperacionDto actualizarRetiroQR(String numATM, String checksum) throws Exception {
		    crearConexion();
		    OperacionDto operacionDto = new OperacionDto();
		    UpdateResult updateResult =null;
		    Bson filter =and(eq("atm_id", numATM), eq("checksum_id",checksum), eq("status_type", 1));
		  	    
			MongoCollection<Document> lstResultado= mdb.getCollection(Constantes.COLLECTION_RETIRO_QR);
			FindIterable<Document> fitd =lstResultado.find(filter).limit(5); 
			if(lstResultado!=null&&lstResultado.count()>0) {
				for (Document document : fitd) {
					System.out.println("iterable "+document.toJson());
					Document query = new Document();
			        query.append("_id",document.get("_id"));
			        Document setData = new Document();
			       // setData.append("atm_id", "0013");
			        
			        setData.append("key_em_number", "3251");
			        setData.append("mobile_number", "983605966");
			        
			        
			        
			        
			        /*Document setData = new Document();
			        setData.append("status_type", 2).append("status_desc", "Cobrado");*/
			        Document update = new Document();
			        update.append("$set", setData);
			        
			        updateResult =lstResultado.updateOne(query, update);
			        System.out.println("estado "+updateResult.getModifiedCount());
				}
			}
			operacionDto.setEstado((updateResult!=null||updateResult.getModifiedCount()!=0)?"NOOK":"OK"   );
			cerrarConexion(mongoClient);
		return operacionDto;
		  
	  }

	  
	  public OperacionDto insertarRetiroQR(String numATM, String checksum,Integer numCel,
			                               Integer claveem,String correo,Integer monto) throws Exception {
		    crearConexion();
		    OperacionDto operacionDto = new OperacionDto();
		
		  	    
			MongoCollection<Document> mongoCollection= mdb.getCollection(Constantes.COLLECTION_RETIRO_QR);
	
			
			        Document setData = new Document();
			        
			        setData.append("atm_id", numATM);
			        setData.append("checksum_id", checksum);
			        setData.append("mobile_number", numCel);
			        setData.append("key_em_number", claveem);
			        setData.append("operation_amount", monto);
			        setData.append("operation_currency_code","PEN");
			        setData.append("account_balance_amount", "110486700200397803");
			        setData.append("account_type", 2);
			        setData.append("account_type_desc", "Ahorro");
			        setData.append("customer_name","Monica La Cruz");
			        setData.append("email_address", correo);
			        setData.append("status_type", 1);
			        setData.append("status_desc", "Pendiente");
			        setData.append("operation_number", 872);
			        setData.append("operation_identification_id", "OK");
			        setData.append("operation_description_desc", "Operacion Exitosa");
			        
			        mongoCollection.insertOne(setData);
				
			
			//operacionDto.setEstado((updateResult!=null||updateResult.getModifiedCount()!=0)?"NOOK":"OK"   );
			 cerrarConexion(mongoClient);
		return operacionDto;
		  
	  }


	  

}
