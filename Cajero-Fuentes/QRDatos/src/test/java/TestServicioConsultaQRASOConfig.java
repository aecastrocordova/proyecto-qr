

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.aso.servicios.cliente.base.exception.ValidacionExceptionBBVA;
import pe.bbva.aso.servicios.retiroqr.config.RetiroQRASOConfig;
import pe.bbva.retiroqr.bean.RequestConsultaRQRBean;
import pe.bbva.retiroqr.bean.RetiroQRBean;

public class TestServicioConsultaQRASOConfig {
	final static Logger logger = (Logger) LoggerFactory.getLogger(TestServicioConsultaQRASOConfig.class);
	

	 
	public static void main(String[] args) {
		logger.info("TestServicioConsultaQRASOConfig consultar - inicio: ");
		

		RetiroQRASOConfig  obj = new RetiroQRASOConfig();
		RequestConsultaRQRBean filtro = new RequestConsultaRQRBean();
		filtro.setAtm_id("0013");
		filtro.setChsum("12/09/2019 15:58:10:46");
		filtro.setEstado(1);

		RetiroQRBean response = null;
		
		try {
			response = obj.consultarRetiroQR(filtro);
			System.out.println("response " +response.isExiste());
			/*	if(!response.hasError()){
					data = response.getData();
					System.out.println("data "+ data.get(0).getCustomerId());
				}else {
					System.out.println(" ErrorCode: " + response.getErrorCode());
					System.out.println(" ErrorMessage: " + response.getErrorMessage());
				}
			}*/
		} catch (ServiceExceptionBBVA e) {
			if(e instanceof ValidacionExceptionBBVA) {
				ValidacionExceptionBBVA ev = (ValidacionExceptionBBVA) e;
				System.out.println("Handling validation exception getCode : " + ev.getErrores().getFieldError().getCode());
				System.out.println("Handling validation exception getField: " + ev.getErrores().getFieldError().getField());
				System.out.println("Handling validation exception : " + ev.getErrores().getFieldError().getField() + " " + ev.getErrores().getFieldError().getCode());
			}else {
				System.out.println("Handling any exception: " + e.getMessage());
			}		
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
