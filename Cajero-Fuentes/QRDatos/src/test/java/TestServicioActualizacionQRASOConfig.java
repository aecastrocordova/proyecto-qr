

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import pe.bbva.aso.servicios.cliente.base.exception.ServiceExceptionBBVA;
import pe.bbva.aso.servicios.cliente.base.exception.ValidacionExceptionBBVA;
import pe.bbva.aso.servicios.retiroqr.config.RetiroQRASOConfig;
import pe.bbva.retiroqr.dto.EstadoUpdateDto;
import pe.bbva.retiroqr.dto.OperacionDto;
import pe.bbva.retiroqr.dto.RequestUpdateRetiroQRDto;

public class TestServicioActualizacionQRASOConfig {
	final static Logger logger = (Logger) LoggerFactory.getLogger(TestServicioActualizacionQRASOConfig.class);
	

	 
	public static void main(String[] args) {
		logger.info("TestServicioActualizacionQRASOConfig consultar - inicio: ");
		

		RetiroQRASOConfig  obj = new RetiroQRASOConfig();
		RequestUpdateRetiroQRDto filtro = new RequestUpdateRetiroQRDto();
		filtro.setAtm_id("7777");
		filtro.setChsum("28/08/2019 17:05:56:325");
		
		EstadoUpdateDto datos = new EstadoUpdateDto();
		datos.setStatus_type(2);
		datos.setStatus_desc("Cobrado");
		filtro.setDatos(datos);

		OperacionDto response = null;
		
		try {
			response = obj.actualizarRetiroQR(filtro);
			System.out.println("response " +response.getEstado());
			/*	if(!response.hasError()){
					data = response.getData();
					System.out.println("data "+ data.get(0).getCustomerId());
				}else {
					System.out.println(" ErrorCode: " + response.getErrorCode());
					System.out.println(" ErrorMessage: " + response.getErrorMessage());
				}
			}*/
		} catch (ServiceExceptionBBVA e) {
			if(e instanceof ValidacionExceptionBBVA) {
				ValidacionExceptionBBVA ev = (ValidacionExceptionBBVA) e;
				System.out.println("Handling validation exception getCode : " + ev.getErrores().getFieldError().getCode());
				System.out.println("Handling validation exception getField: " + ev.getErrores().getFieldError().getField());
				System.out.println("Handling validation exception : " + ev.getErrores().getFieldError().getField() + " " + ev.getErrores().getFieldError().getCode());
			}else {
				System.out.println("Handling any exception: " + e.getMessage());
			}		
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
